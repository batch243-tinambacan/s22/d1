// console.log("Yow!")

/*
JS has built-in function and methods for arrays; This asllows us to manipulate and access array elements
*/

// Mutator Methods are functions that mutate or change an array after they're created; This methods manipulate the original array performing various tasks such as adding and removing elements

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];
console.log(fruits)

// push() -adds an element in the end of an array and returns the array's length. Syntax: arrayName.push(elementsToBeAdded);

	console.log("Current Array fruits[]: ");
	console.log(fruits);

	// fruits.push('Mango');
	console.log("Mutated array from push method: ")
	console.log(fruits);

	let fruitsLength =fruits.push('Mango')
	console.log(fruitsLength)

	fruitsLength = fruits.push("Avocado", "Guava");
	console.log("Mutated array from second push")
	console.log(fruits);
	console.log(fruitsLength)



 // Pop() - Removes the last elementin and array and returns the removed element

	console.log("Current Array fruits[]: ");
	console.log(fruits);

	fruits.pop();
	console.log("Mutated array after the pop method: ");
	console.log(fruits)

	//Reassigning a variable, it will show the removed element
	let removedFruit = fruits.pop() 
	console.log(fruits)
	console.log(removedFruit)

// Unshift() - adds one or more element at the beginning of an array AND return the present length.

	console.log("Current array fruits[]: ");
	console.log(fruits);

	// fruits.unshift('Lime', 'banana');
	fruitsLength = fruits.unshift('Lime', 'banana') //reassign value to get length
	console.log("Mutated Array after the unshift method:")
	console.log(fruits)
	console.log(fruitsLength)


// shift() - removes an element at the beginning of an array AND it returns the removed element.

	console.log("Current array fruits[]: ");
	console.log(fruits);

	// fruits.shift();
	removedFruit = fruits.shift(); //return the removed element
	console.log("Mutated Array after the shift method:")
	console.log(fruits)
	console.log(removedFruit)

// Splice - simultaneously removes element from a specified index number and adds elements.
// syntax: arrayName.splice(startingIndex, deleteCount, elementsToBeAdded )

	console.log("Current array fruits[]: ");
	console.log(fruits);


	fruits.splice(1,1,"Lime"); //if (1,2), orange will be removed too
	console.log("Mutated Array after the splice method:")
	console.log(fruits);

	fruits.splice(5,0, "Cherry"); //nothing is removed or replaced; Cherry was inserted in index 5
	console.log(fruits);

	fruits.splice(3,0, "Durian", "Santol")
	console.log(fruits); //nothing is removed or replaced; Durian and Santol were inserted starting from index 3

	//Splice for removing, .splice(0,1,)
	fruits.splice(0,2,); // the first 2 elements were removed
	console.log(fruits);


// sort() - rearranges the array elements in alphanumeric order. 

	console.log("Current array fruits[]: ");
	console.log(fruits);

	fruits.sort();
	// console.log(fruits.sort())
	console.log("Mutated Array after the sort method:")
	console.log(fruits);

	// The sort method is used for more complicated functions.


// reverse() - it reverses the order of array elements. The first index or [0] will be placed in the last index and vice versa.

	console.log("Current array fruits[]: ");
	console.log(fruits);

	// console.log("Return of reverse() method")
	// console.log(fruits.reverse())

	fruits.reverse()
	console.log("Mutated Array after the reverse method:")
	console.log(fruits);

// NON-MUTATOR METHODS - are functions that do not modify or change an array after they're created.


let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE']

// indexOf() - returns the index number of the first matching element found in array. If no match was found, the result will be -1; The search process will be done from first element proceeding to the last element.
// syntax: arrayName.indexOF(searchValue);
		// arrayName.indexOf(searchValue, startingIndex); 


console.log(countries.indexOf('PH'));
console.log(countries.indexOf('BR'));

// arrayName.indexOf(searchValue, startingIndex); 
console.log(countries.indexOf('PH', 2)); //will return 5, the index number of the first PH after the second index

// lastIndexOf() - returns the index number of the last matching element found in an array

console.log(countries.lastIndexOf('PH')); //will return 5, the index number of last PH

// Slice() - portion/slices from an array and returns a new array starting from the stated index

let slicedArrayA = countries.slice(2);
console.log(slicedArrayA);
console.log(countries);

// Slicing off elements from a specified index to another index

let slicedArrayB = countries.slice(1,5); // 5-1 = 4 means 4 elements will be gathered
console.log(slicedArrayB)

// slicing off elements starting from the last element of an array
// last three elements will get sliced
let slicedArrayC = countries.slice(-3);
console.log(slicedArrayC)

// let slicedArrayC = countries.slice(-3, -1)
// console.log(slicedArrayC)


// toString() - returns an array as string separated by commas

let stringedArray = countries.toString();
console.log(stringedArray)

// concate() - combines arrays and returnds the combined result
// syntax:
// arrayA.concate(arrayB);

let taskArrayA = ['drink HTML', 'eat javascript'];
let taskArrayB = ['inhale CSS', 'breathe SASS'];
let taskArrayC = ['get fit', 'be node'];


let tasks = taskArrayA.concat(taskArrayB); //arrayB will be added as last indeces in arrayA
console.log('result from concate method:');
console.log(tasks)

// combining multiple arrays
console.log("Result from concat method:")
let allTask = taskArrayA.concat(taskArrayB, taskArrayC)
console.log(allTask)

// Combine array with elements
let combinedTasks = taskArrayA.concat('smell express', 'throw react')
console.log("Result from concate method")
console.log(combinedTasks)


//join() - returns an array as string separated by specified separator string

let users = ['John', 'Jane', 'Joe',"Robert"];
console.log(users.join());
console.log(users.join('; ')) //seprate by ;


// ITERATION METHOD - are loop designed to perform repetitive task on arrays; Loops over all elements in array.


// foreach() - similar to for loop that iterates on each of array element; for each element in the array, the function in the foreach() will be run.
// Syntax: arrayName.forEach(function(indivElement){
		// statement/s
// })

console.log(allTask);

allTask.forEach(function(task){
	console.log(task);
})

//filter elements with more than 10 characters
let filteredTasks = []; 

allTask.forEach(function(task){
	if(task.length > 10){
		filteredTasks.push(task);
	}
})
console.log(filteredTasks);


// map() - iterates on each element and returns new array with different values depending on the result of the function's operation. No return value when contain in a variable
/* Syntax:
	let/const resultArray = arrayName.map(function(elements){
		statements;
		return;
	})
*/
	let numbers = [1,2,3,4,5];

	let numberMap = numbers.map(function(number){
		return number*number;
	})
	console.log("original Array: ");
	console.log(numbers);
	console.log("Result of map():")
	console.log(numberMap)

// Every() - checks if all elements in an array meet the given condition. This is useful when validatin data stored in arrays, especially when dealing with large amounts of data; Will return a true value if all meet the condition and false if otherwise.
/* Syntax:
			let/const resultArray = arrayName.every(function(element){
				return condition
			})
*/

console.log(numbers);
let allValid = numbers.every(function(number){
	return (number<3); // will return false because number[] contains element greater than 3
})

console.log(allValid);

// some() - checks if at least one element in the array meets the given condition; returns a true value if at least one element meets the condition and false if none.
/* Syntax:
			let/const resultArray = arrayName.some(function(element){
				return condition
			})
*/

console.log(numbers);
let somevalid = numbers.some(function(number){
	return (number<3)
})
console.log(somevalid)

// filter() - return new array that contains the elements which meets the given condition; It will return an empty array if no elements were found.

/* Syntax:
			let/const resultArray = arrayName.filter(function(element){
				return condition
			})
*/

console.log(numbers);
let filterValid = numbers.filter(function(number){
	return (number > 3)
})

console.log(filterValid)

// include() - checks if the arguments passed can be found in the array. It returns boolean which can be saved in variable; returns true if the argument is found in the array; false if is not. Note: Case sensitive
// Syntax: arrayName.includes(argument)

let products = ['Mouse', 'Keyoboard', 'Laptop', 'Monitor'];

let productFound1 = products.includes('Mouse');
console.log(productFound1) //true

let productFound2 = products.includes('Headset');
console.log(productFound2) //false


// Reduce() - it will evaluate elements from left to right and returns/reduces the array into a single value.
/* Syntax:
			let/const resultValue = arrayName.reduce(function(accumulator, currentValue)){
				return condition
			});
*/

console.log(numbers);
let total = numbers.reduce(function(x,y){
	console.log("This is the value of x: " + x);
	console.log("This is the value of y:" + y);
	return x+y
})
	console.log(total)

	